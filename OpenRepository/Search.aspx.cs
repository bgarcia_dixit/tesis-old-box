﻿using OpenRepository.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OpenRepository
{
    public partial class Search : System.Web.UI.Page
    {
        private String search;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["word"] == null || Request.QueryString["word"].Length == 0)
                Response.Redirect("~/Default.aspx");
            
            this.search = Request.QueryString["word"];

            UnitOfWork uow = new UnitOfWork(SessionManager.Instance);
            IList<Usuario> usuarios = uow.Users.getAll(new Usuario(), "c.Name LIKE '%" + search + "%' OR c.Lastname LIKE '%"+ search +"%' OR c.Email like '%"+ search +"%'" );
            IList<Project> proyectos = uow.Projects.getAll(new Project(), "c.Name LIKE '%" + search + "%' OR c.Summary LIKE '%" + search + "%' OR c.User.Name LIKE '%" + search + "%' OR c.User.Lastname LIKE '%" + search + "%' OR c.User.Email like '%" + search + "%'");

            if (usuarios.Count > 0)
                userSearchDiv.Visible = true;
            if (proyectos.Count > 0)
                projectSearchDiv.Visible = true;

            foreach(Usuario item in usuarios)
            {
                TableRow tRow = new TableRow();
                TableUsers.Rows.Add(tRow);

                TableCell tCellNombre = new TableCell();
                HyperLink link = new HyperLink();
                link.NavigateUrl = "~/Profile.aspx?userProfile=" + item.Email;
                link.Text = item.Name + " " + item.Lastname;
                tCellNombre.Controls.Add(link);

                TableCell tCellEmail = new TableCell();
                tCellEmail.Text = item.Email.ToString();

                tRow.Cells.Add(tCellNombre);
                tRow.Cells.Add(tCellEmail);
            }

            foreach (Project item in proyectos)
            {
                TableRow tRow = new TableRow();
                TableProjects.Rows.Add(tRow);

                TableCell tCellProject = new TableCell();
                HyperLink link = new HyperLink();
                link.NavigateUrl = "~/Thesis.aspx?thesisID=" + item.Id;
                link.Text = item.Name;
                tCellProject.Controls.Add(link);

                TableCell tCellCarreer = new TableCell();
                tCellCarreer.Text = item.Carreer.Name;

                TableCell tCellAutor = new TableCell();
                tCellAutor.Text = item.User.Name + " " + item.User.Lastname;

                tRow.Cells.Add(tCellProject);
                tRow.Cells.Add(tCellCarreer);
                tRow.Cells.Add(tCellAutor);
            }
        }
    }
}