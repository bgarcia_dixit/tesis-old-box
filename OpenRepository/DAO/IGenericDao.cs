﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenRepository.DAO
{
    interface IGenericDao<T,L>
    {
        IList<T> getAll(T c);
        IList<T> getAll(T c, string field, string word);
        IList<T> getAll(T c, string condicion);
        IList<T> search(T c, string field, string word);
        IList<T> getAll(string strquery, int pageNumber, int pageSize);
        long getMaxPage(String strquery, int pageSize);
        IList<T> getAllOrderBy(T c, string orderBy);
        T get(L id);
        void save(T pojo);
        void update(T pojo);
        void delete(T pojo);
    }
}
