﻿using OpenRepository.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenRepository.DAO
{
    interface IUsuarioDao : IGenericDao<Usuario, long>
    {
        IList<Usuario> readAll(int status);
        
        Usuario readAll(String Username, String Password);

        bool existEmail(String email);
        Usuario buscarPerfil(String Username);

        long getTotal();
        long getMes();
        long getDia();
    }
}
