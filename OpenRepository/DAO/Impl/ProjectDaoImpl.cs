﻿using NHibernate;
using NHibernate.Cfg;
using OpenRepository.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenRepository.DAO.Impl
{
    public class ProjectDaoImpl : GenericDaoImpl<Project, long>, IProjectDao 
    {
        public ProjectDaoImpl(ISession session)
        {
            this.session = session;
        }

        public long getTotal()
        {
            long cantidad = this.session.CreateQuery("SELECT COUNT(*) FROM Project").UniqueResult<long>();
            return cantidad;
        }

        public long getMes()
        {
            long cantidad = this.session.CreateQuery("SELECT COUNT(*) FROM Project u WHERE YEAR(u.Date) = YEAR(CURRENT_DATE) AND MONTH(u.Date) = MONTH(CURRENT_DATE)").UniqueResult<long>();
            return cantidad;
        }

        public long getDia()
        {
            long cantidad = this.session.CreateQuery("SELECT COUNT(*) FROM Project u WHERE YEAR(u.Date) = YEAR(CURRENT_DATE) AND MONTH(u.Date) = MONTH(CURRENT_DATE) AND DAY(u.Date) = DAY(CURRENT_DATE)").UniqueResult<long>();
            return cantidad;
        }

        public long getUser(Usuario user)
        {
            long cantidad = this.session.CreateQuery("SELECT COUNT(*) FROM Project u WHERE u.User = ?").SetParameter(0, user).UniqueResult<long>();
            return cantidad;
        }
    }
}