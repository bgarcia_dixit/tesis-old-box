﻿using NHibernate;
using NHibernate.Cfg;
using OpenRepository.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenRepository.DAO.Impl
{
    public class UsuarioDaoImpl : GenericDaoImpl<Usuario, long>, IUsuarioDao
    {
        public UsuarioDaoImpl(ISession session)
        {
            this.session = session;
        }

        public IList<Usuario> readAll(int status)
        {
            try
            {
                IList<Usuario> lista = this.session.CreateQuery("FROM Usuario c WHERE c.Status < ?").SetParameter(0, status).List<Usuario>();
                return lista;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool existEmail(string email)
        {
            bool exist = false;
            IList<Usuario> lista;

            try
            {
                lista = this.session.CreateQuery("FROM Usuario u WHERE u.Email = ?").SetParameter(0, email).List<Usuario>();
            }
            catch (Exception e)
            {
                throw e;
            }
            if (lista.Count > 0)
            {
                exist = true;
            }

            return exist;
        }


        public Usuario readAll(string Username, string Password)
        {

            Usuario user;
            try
            {
                user = this.session.CreateQuery("FROM Usuario u WHERE u.Email = ? AND u.Password = ?").SetParameter(0, Username).SetParameter(1, Password).UniqueResult<Usuario>();
                return user;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public Usuario buscarPerfil(string Username)
        {
            Usuario userProfile = this.session.CreateQuery("FROM Usuario u WHERE u.Email = ?").SetParameter(0, Username).UniqueResult<Usuario>();
            return userProfile;
        }

        public long getTotal()
        {
            long cantidad = this.session.CreateQuery("SELECT COUNT(*) FROM Usuario").UniqueResult<long>();
            return cantidad;
        }
        public long getMes()
        {
            long cantidad = this.session.CreateQuery("SELECT COUNT(*) FROM Usuario u WHERE YEAR(u.Date) = YEAR(CURRENT_DATE) AND MONTH(u.Date) = MONTH(CURRENT_DATE)").UniqueResult<long>();
            return cantidad;
        }
        public long getDia()
        {
            long cantidad = this.session.CreateQuery("SELECT COUNT(*) FROM Usuario u WHERE YEAR(u.Date) = YEAR(CURRENT_DATE) AND MONTH(u.Date) = MONTH(CURRENT_DATE) AND DAY(u.Date) = DAY(CURRENT_DATE)").UniqueResult<long>();
            return cantidad;
        }
    }
}