﻿using NHibernate;
using NHibernate.Cfg;
using OpenRepository.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenRepository.DAO.Impl
{
    public class BanDaoImpl : GenericDaoImpl<Ban, long>, IBanDao
    {
        public BanDaoImpl(ISession session)
        {
            this.session = session;
        }

        public Ban searchBan(Usuario userBan)
        {
            try
            {
                Ban baneo = this.session.CreateQuery("FROM Ban c WHERE c.User = ?").SetParameter(0,userBan).UniqueResult<Ban>();
                return baneo;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}