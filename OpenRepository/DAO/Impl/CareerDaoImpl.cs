﻿using NHibernate;
using NHibernate.Cfg;
using OpenRepository.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenRepository.DAO.Impl
{
    public class CareerDaoImpl : GenericDaoImpl<Career, long>, ICareerDao
    {
        public CareerDaoImpl(ISession session)
        {
            this.session = session;
        }

        public Career search(long Id)
        {
            try
            {
                Career carrera = this.session.CreateQuery("FROM Career c WHERE c.Id = ?").SetParameter(0,Id).UniqueResult<Career>();
                return carrera;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}