﻿using NHibernate;
using NHibernate.Cfg;
using OpenRepository.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenRepository.DAO.Impl
{
    public class GenericDaoImpl<T, L> : IGenericDao<T, L> where T : class
    {
        protected ISession session;

        public GenericDaoImpl()
        {

        }

        public IList<T> getAll(string strquery, int pageNumber, int pageSize)
        {
            try
            {
                List<T> lista = new List<T>();
                IQuery query = session.CreateQuery(strquery).SetCacheable(true).SetFlushMode(FlushMode.Auto);
                query.SetFirstResult((pageNumber - 1) * pageSize);
                query.SetMaxResults(pageSize);
                query.List(lista);
                return lista;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public long getMaxPage(String strquery, int pageSize)
        {
            try
            {
                IQuery q = session.CreateQuery(strquery).SetCacheable(true).SetFlushMode(FlushMode.Auto);
                Int64 iss = q.UniqueResult<Int64>();
                return (iss / pageSize) + 1;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public IList<T> getAll(T c)
        {
            try
            {
                IList<T> list = this.session.CreateQuery(
                        " SELECT c FROM " + c.GetType().Name + " c").SetCacheable(true).SetFlushMode(FlushMode.Auto).List<T>();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IList<T> getAll(T c, string field, string word)
        {
            try
            {
                IList<T> list = this.session.CreateQuery(
                        " SELECT c FROM " + c.GetType().Name + " c where c." + field + " LIKE '%" + word + "%'").SetCacheable(true).SetFlushMode(FlushMode.Auto).List<T>();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public IList<T> getAll(T c, string condicion)
        {
            try
            {
                IList<T> list = this.session.CreateQuery(
                        " SELECT c FROM " + c.GetType().Name + " c where " + condicion).SetCacheable(true).SetFlushMode(FlushMode.Auto).List<T>();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public IList<T> search(T c, string field, string word)
        {
            try
            {
                IList<T> list = this.session.CreateQuery(
                        " SELECT c FROM " + c.GetType().Name + " c where " + field).SetCacheable(true).SetFlushMode(FlushMode.Auto).List<T>();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IList<T> getAllOrderBy(T c, string orderBy)
        {
            try
            {
                IList<T> list = this.session.CreateQuery(
                        "SELECT c FROM " + c.GetType().Name + " c  Order By c." + orderBy.Trim()).SetCacheable(true).SetFlushMode(FlushMode.Auto).List<T>();
                this.session.Flush();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public T get(L id)
        {
            return this.session.Get<T>(id);
        }
        public void save(T pojo)
        {
            ITransaction transaction = this.session.BeginTransaction();
            try
            {
                this.session.SaveOrUpdate(pojo);
                transaction.Commit();
                this.session.Flush();
                this.session.Evict(pojo); //atento
            }
            catch
            {
                transaction.Rollback();
                throw;
            }
        }

        public void update(T pojo)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                this.session.Update(pojo);
                transaction.Commit();
                session.Flush();
            }
            catch
            {
                transaction.Rollback();
                throw;
            }
        }
        public void delete(T pojo)
        {
            ITransaction transaction = session.BeginTransaction();
            this.session.Delete(pojo);
            transaction.Commit();
            session.Flush();
        }
    }
}