﻿using OpenRepository.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenRepository.DAO
{
    interface IBanDao : IGenericDao<Ban, long>
    {
        Ban searchBan(Usuario userBan);
    }
}
