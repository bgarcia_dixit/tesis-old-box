﻿using OpenRepository.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenRepository.DAO
{
    interface IProjectDao : IGenericDao<Project, long>
    {
        long getTotal();
        long getMes();
        long getDia();
        long getUser(Usuario user);
    }
}
