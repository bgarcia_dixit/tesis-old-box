﻿using OpenRepository.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenRepository.DAO
{
    interface ICareerDao : IGenericDao<Career, long>
    {
        Career search(long Id);
    }
}
