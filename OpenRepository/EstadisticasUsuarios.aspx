﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="EstadisticasUsuarios.aspx.cs" Inherits="OpenRepository.EstadisticasUsuarios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>Tabla de usuarios</h3>
    <div>
        <asp:HyperLink ID="BackLink" CssClass="btn btn-default" runat="server" NavigateUrl="~/Estadisticas.aspx">Atras</asp:HyperLink>
    </div>
    <asp:Table ID="TableUsers" runat="server" class="table table-striped table-hover ">
        <asp:TableRow CssClass="info">
            <asp:TableCell>ID</asp:TableCell>
            <asp:TableCell>Nombre</asp:TableCell>
            <asp:TableCell>Apellido</asp:TableCell>
            <asp:TableCell>Fecha Registro</asp:TableCell>
            <asp:TableCell>Estado</asp:TableCell>
            <asp:TableCell>Tesis Subidas</asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
