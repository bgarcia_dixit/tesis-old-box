﻿using OpenRepository.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OpenRepository
{
    public partial class Thesis : System.Web.UI.Page
    {
        long thesisID;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Redireccion a principal, cosa de programadores...
            if (Request.QueryString["thesisID"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            thesisID = Int64.Parse(Request.QueryString["thesisID"]);
            UnitOfWork uow = new UnitOfWork(SessionManager.Instance);

            /* TRAER LA TESIS */
            Project thesis = uow.Projects.get(thesisID);

            titulo.Text = thesis.Name;
            resumen.Text = thesis.Summary;
            PdfViewer.Src = "Archives/" + thesis.ArchivesList[0].Name;

            if (thesis.User.ImgProfile != null)
            {
                ImageProfile.ImageUrl = thesis.User.ImgProfile;
            }
            LabelProfileName.Text = thesis.User.Name + " " + thesis.User.Lastname;
            LabelProfileName.NavigateUrl = "~/Profile.aspx?userProfile=" + thesis.User.Email;

        }
    }
}