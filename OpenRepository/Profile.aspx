﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="OpenRepository.Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!--
User Profile Sidebar by @keenthemes
A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
Licensed under MIT
-->
    <div class="row profile">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <asp:Image ID="ImageProfile" runat="server" ImageUrl="~/Img/Resources/Perfil.jpg" class="img-responsive" alt="" />
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        <asp:Label ID="LabelProfileName" runat="server" Text="Perfil Incorrecto"></asp:Label>
                    </div>
                    <div class="profile-usertitle-job">
                        <asp:Label ID="LabelProfileStatus" runat="server" Text="Error."></asp:Label>
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR MENU -->
                <div runat="server" class="profile-usermenu">
                    <ul class="nav" runat="server">
                        <li id="SidePrincipal" runat="server" class="active">
                            <asp:LinkButton ID="LinkButtonMain" runat="server" OnClick="LinkButtonMain_Click">
                                <i class="glyphicon glyphicon-home"></i>
                            Principal </asp:LinkButton>
                        </li>
                        <li id="SideCfg" runat="server">
                            <asp:LinkButton ID="LinkButtonCfg" runat="server" OnClick="LinkButtonCfg_Click">
                                <i class="glyphicon glyphicon-cog"></i>
                                Configuracion </asp:LinkButton>
                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
        </div>
        <div class="col-md-9">
            <div class="profile-content">
                <!-- DIV PRINCIPAL DE PERFIL -->
                <div id="ProfileMain" runat="server">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <span runat="server" class="glyphicon glyphicon-envelope"></span>
                            <asp:Label ID="LabelEmail" runat="server">Email</asp:Label>
                        </div>
                    </div>

                    <div runat="server" visible="false" id="divBan" class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title">Usuario Bloqueado</h3>
                        </div>
                        <div class="panel-body">
                            <span runat="server" class="glyphicon glyphicon-user"></span>
                            <asp:Label Text="Administrador: " runat="server" />
                            <asp:Label ID="LabelAdmin" runat="server">Admin</asp:Label><br /><br />

                            <asp:Label Text="Por más informacion contactarse a:" runat="server" /><br />
                            <span runat="server" class="glyphicon glyphicon-envelope"></span>
                            <asp:Label ID="LabelEmailAdmin" runat="server">EmailAdmin</asp:Label><br />
                            <div runat="server" visible="false" id="divSAdmin">
                                <span runat="server" class="glyphicon glyphicon-envelope"></span>
                                <asp:Label ID="LabelEmailSadmin" runat="server">alexis_bianchi@hotmail.com</asp:Label>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- DIV CONFIGURACION DE PERFIL -->
                <div id="ProfileCfgDiv" runat="server">
                    <div class="form-horizontal">
                        <fieldset>
                            <legend>Configuración de Perfil</legend>

                            <!-- IMG DE PERFIL -->
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Imagen</label>
                                <div class="col-lg-10">
                                    <asp:FileUpload ID="FileUploadImgProfile" runat="server" data-show-upload="false" accept="image/*" class="file" />
                                </div>
                            </div>

                            <!-- Nombre -->
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Nombre</label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBoxName" class="form-control" placeholder="Nombre" runat="server" Text="Nombre"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorName" runat="server" ErrorMessage="Debe ingresar su nombre." ControlToValidate="TextBoxName" Display="Dynamic" meta:resourcekey="validarNameVacioResource1" ClientIDMode="Inherit" CssClass="label label-danger">Debe ingresar su nombre.</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <!-- Apellido -->
                            <div class="form-group">
                                <label for="inputText" class="col-lg-2 control-label">Apellido</label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBoxLastName" class="form-control" runat="server" placeholder="Apellido" Text="Apellido"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorLastname" runat="server" ErrorMessage="Ingrese un apellido." ControlToValidate="TextBoxLastName" Display="Dynamic" meta:resourcekey="validarLastnameVacioResource1" ClientIDMode="Inherit" CssClass="label label-danger">Debe ingresar su apellido.</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <!-- Email (no se puede cambiar) -->
                            <div class="form-group">
                                <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBoxEmail" class="form-control" runat="server" ReadOnly="true" placeholder="ejemplo@opensource.com"></asp:TextBox>
                                </div>
                            </div>
                            <!-- Botones -->
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <asp:Button ID="BtnCancel" runat="server" CssClass="btn btn-default" Text="Cancelar" OnClick="BtnCancel_Click" />
                                    <asp:Button ID="BtnSave" runat="server" CssClass="btn btn-primary" Text="Guardar" OnClick="BtnSave_Click" />
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
