﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Registro.aspx.cs" Inherits="OpenRepository.Registro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="container">
        <div class="row centered-form">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Registro</h3>
                    </div>
                    <div class="panel-body">
                        <div role="form">

                            <div id="AlertNotificationDivRegistrado" class="form-group alert alert-success" role="alert" runat="server">
                                <asp:Label ID="AlertNotificationBoxRegistrado" runat="server" Text="Registro Correcto" />
                                <asp:HyperLink ID="HyperLink2" class="alert-link" runat="server" NavigateUrl="Default.aspx">Ir al inicio.</asp:HyperLink>
                            </div>

                            <div class="form-group">
                                <asp:TextBox ID="email" runat="server" name="email" placeholder="Correo Electronico" class="form-control"></asp:TextBox>
                                <div id="AlertNotificationDivEmail" class="alert alert-danger" role="alert" runat="server">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Error:</span>
                                    <asp:Label ID="AlertNotificationBoxEmail" runat="server" Text="El email ya esta en uso." />
                                    <asp:HyperLink ID="HyperLink1" class="alert-link" runat="server" NavigateUrl="#">Recuperar Clave</asp:HyperLink>
                                </div>
                                <asp:RequiredFieldValidator ID="validarEmailVacio" runat="server" ErrorMessage="Ingrese un email." ControlToValidate="email" Display="Dynamic" meta:resourcekey="validarEmailVacioResource1" ClientIDMode="Inherit" CssClass="label label-danger">Ingrese un email.</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="validarFormatoEmail" runat="server" ErrorMessage="El email no es correcto." ControlToValidate="email" Display="Dynamic" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ValidationGroup="AllValidators" meta:resourcekey="validarFormatoEmailResource1" CssClass="label label-danger">Email incorrecto</asp:RegularExpressionValidator>
                            </div>

                            <div class="form-group">
                                <asp:TextBox ID="name" runat="server" name="name" placeholder="Nombre" class="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorName" runat="server" ErrorMessage="Debe ingresar su nombre." ControlToValidate="name" Display="Dynamic" meta:resourcekey="validarNameVacioResource1" ClientIDMode="Inherit" CssClass="label label-danger">Debe ingresar su nombre.</asp:RequiredFieldValidator>
                            </div>

                            <div class="form-group">
                                <asp:TextBox ID="lastname" runat="server" name="lastname" placeholder="Apellido" class="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorLastname" runat="server" ErrorMessage="Ingrese un apellido." ControlToValidate="lastname" Display="Dynamic" meta:resourcekey="validarLastnameVacioResource1" ClientIDMode="Inherit" CssClass="label label-danger">Debe ingresar su apellido.</asp:RequiredFieldValidator>
                            </div>

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <asp:TextBox ID="password" type="password" runat="server" name="pass" placeholder="Clave" class="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <asp:TextBox ID="repassword" runat="server" type="password" name="repass" placeholder="Repetir Clave" class="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" runat="server" ErrorMessage="Ingrese una clave." ControlToValidate="password" Display="Dynamic" meta:resourcekey="validarPasswordVacioResource1" ClientIDMode="Inherit" CssClass="label label-danger">Ingrese una contraseña.</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidatorPassword" runat="server" ErrorMessage="CompareValidatorPasswordError" ControlToCompare="repassword" ControlToValidate="password" CssClass="label label-danger" Display="Dynamic">Las claves no coinciden.</asp:CompareValidator>
                            <asp:Button ID="BtnRegistro" runat="server" Text="Registrarse" type="submit" name="registro" class="btn btn-primary btn-block" value="Registro" OnClick="BtnRegistro_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
