﻿using OpenRepository.Clases;
using OpenRepository.DAO;
using OpenRepository.DAO.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OpenRepository
{
    public partial class Registro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AlertNotificationDivEmail.Visible = false;
            AlertNotificationBoxEmail.Visible = false;
            AlertNotificationBoxRegistrado.Visible = false;
            AlertNotificationDivRegistrado.Visible = false;
        }

        protected void BtnRegistro_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                UnitOfWork uow = new UnitOfWork(SessionManager.Instance);

                bool emailexist = uow.Users.existEmail(email.Text);
                bool clave = false;
                bool nombre = false;
                bool apellido = false;

                if (password.Text == repassword.Text)
                {
                    clave = true;
                }
                if (name.Text.Count() > 0)
                {
                    nombre = true;
                }
                if (lastname.Text.Count() > 0)
                {
                    apellido = true;
                }


                if (emailexist == false && clave == true && nombre == true && apellido == true)
                {
                    Usuario userNew = new Usuario();
                    userNew.Password = password.Text;
                    userNew.Email = email.Text;
                    userNew.Name = name.Text;
                    userNew.Lastname = lastname.Text;
                    userNew.Date = DateTime.Now;

                    userNew.Status = 1;

                    //user.save(userNew);
                    uow.Users.save(userNew);

                    password.Text = "";
                    repassword.Text = "";
                    email.Text = "";
                    AlertNotificationBoxRegistrado.Visible = true;
                    AlertNotificationDivRegistrado.Visible = true;
                }
                else
                {
                    if (emailexist == true)
                    {
                        AlertNotificationDivEmail.Visible = true;
                        AlertNotificationBoxEmail.Visible = true;
                    }
                }
            }
        }
    }
}