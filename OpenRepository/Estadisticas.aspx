﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Estadisticas.aspx.cs" Inherits="OpenRepository.Estadisticas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>Panel de Estadísticas</h3>
    <div class="adminContainer">
        <div class="panel panel-default">
            <div class="panel-heading">Registro de Usuarios</div>
            <div class="panel-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cantidad</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Total</td>
                            <td><asp:Label ID="lblTotalUsuario" runat="server" Text="0"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Este Mes</td>
                            <td><asp:Label ID="lblMesUsuario" runat="server" Text="0"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Hoy</td>
                            <td><asp:Label ID="lblDiaUsuario" runat="server" Text="0"></asp:Label></td>
                        </tr>
                    </tbody>
                </table>
                <asp:HyperLink runat="server" CssClass="btn btn-default" NavigateUrl="~/EstadisticasUsuarios.aspx">Ver Informe</asp:HyperLink>
            </div>
        </div>
    </div>

    <div class="adminContainer">
        <div class="panel panel-default">
            <div class="panel panel-heading">Registro de Tesis</div>
            <div class="panel-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cantidad</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Total</td>
                            <td><asp:Label ID="lblTotalTesis" runat="server" Text="0"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Este Mes</td>
                            <td><asp:Label ID="lblMesTesis" runat="server" Text="0"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Hoy</td>
                            <td><asp:Label ID="lblDiaTesis" runat="server" Text="0"></asp:Label></td>
                        </tr>
                    </tbody>
                </table>
                <asp:HyperLink ID="verInformeTesis" CssClass="btn btn-default" runat="server" NavigateUrl="~/EstadisticasTesis.aspx">Ver Informe</asp:HyperLink>
            </div>
        </div>
    </div>
</asp:Content>
