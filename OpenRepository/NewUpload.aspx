﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="NewUpload.aspx.cs" Inherits="OpenRepository.NewUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="form-horizontal">
        <fieldset>
            <legend>Información del proyecto</legend>
            <div class="form-group">
                <label for="inputTitle" class="col-lg-2 control-label">Titulo</label>
                <div class="col-lg-10">
                    <asp:TextBox ID="InputTitle" class="form-control" runat="server" placeholder="Titulo de tesis"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <label for="inputCarrera" class="col-lg-2 control-label">Carrera</label>
                <div class="col-lg-10">
                    <asp:DropDownList ID="DropDownListCarreras" CssClass="form-control" runat="server"></asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="PDF" class="col-lg-2 control-label">Archivo PDF</label>
                <!-- PDF -->
                <div class="col-lg-10">
                    <asp:FileUpload ID="FileUploadMainPDF" runat="server" data-show-upload="false" accept=".pdf" class="file" />
                </div>
            </div>
            <div class="form-group">
                <label for="textArea" class="col-lg-2 control-label">Resumen</label>
                <div class="col-lg-10">
                    <textarea class="form-control" runat="server" rows="3" id="textAreaSummary"></textarea>
                    <span class="help-block">Indique el resumen de su tesis, sera mostrado en primer instancia de lectura.</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <asp:Button ID="BtnUploadProject" runat="server" CssClass="btn btn-primary" Text="Compartir" OnClick="BtnUploadProject_Click" />
                </div>
            </div>
        </fieldset>
    </div>
</asp:Content>
