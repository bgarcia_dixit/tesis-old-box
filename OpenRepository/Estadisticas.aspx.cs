﻿using OpenRepository.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OpenRepository
{
    public partial class Estadisticas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UnitOfWork uow = new UnitOfWork(SessionManager.Instance);
            long userTotal = uow.Users.getTotal();
            long userMes = uow.Users.getMes();
            long userDia = uow.Users.getDia();

            lblTotalUsuario.Text = userTotal.ToString();
            lblMesUsuario.Text = userMes.ToString();
            lblDiaUsuario.Text = userDia.ToString();

            long tesisTotal = uow.Projects.getTotal();
            long tesisMes = uow.Projects.getMes();
            long tesisDia = uow.Projects.getDia();

            lblTotalTesis.Text = tesisTotal.ToString();
            lblMesTesis.Text = tesisMes.ToString();
            lblDiaTesis.Text = tesisDia.ToString();
        }
    }
}