﻿using OpenRepository.Clases;
using OpenRepository.DAO;
using OpenRepository.DAO.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OpenRepository
{
    public partial class Default : System.Web.UI.Page
    {
        IList<Project> projectsList = new List<Project>();
        UnitOfWork uow = new UnitOfWork(SessionManager.Instance);
        IList<Career> carreerList = new List<Career>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                carreerList = uow.Carreer.getAllOrderBy(new Career(), "Name");

                DropDownListCarreras.DataSource = carreerList;
                DropDownListCarreras.DataTextField = "Name";
                DropDownListCarreras.DataValueField = "Id";
                DropDownListCarreras.DataBind();

                projectsList = uow.Projects.getAll(new Project(), " c.Enable = TRUE ");
                RecargarLista();
            }
             
        }
        protected void filtrar(object sender, EventArgs e)
        {
            projectsList = uow.Projects.getAll(new Project(), " c.Enable = TRUE AND c.Carreer.Id = " + DropDownListCarreras.SelectedValue);
            RecargarLista();
        }
        protected void borrarFiltro(object sender, EventArgs e)
        {
            projectsList = uow.Projects.getAll(new Project(), " c.Enable = TRUE ");
            RecargarLista();   
        }

        protected void RecargarLista()
        {
            foreach (Project item in projectsList)
            {
                TableRow tRow = new TableRow();
                TableProject.Rows.Add(tRow);

                TableCell tCellProject = new TableCell();
                HyperLink link = new HyperLink();
                link.NavigateUrl = "~/Thesis.aspx?thesisID=" + item.Id;

                link.Text = item.Name;
                tCellProject.Controls.Add(link);

                TableCell tCellCarreer = new TableCell();
                tCellCarreer.Text = item.Carreer.Name;

                tRow.Cells.Add(tCellProject);
                tRow.Cells.Add(tCellCarreer);
            }
        }
    }
}