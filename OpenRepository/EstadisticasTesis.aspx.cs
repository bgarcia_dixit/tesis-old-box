﻿using OpenRepository.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OpenRepository
{
    public partial class EstadisticasTesis : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session.Count > 0 && int.Parse(Session["status"].ToString()) > 1)
            {
                UnitOfWork uow = new UnitOfWork(SessionManager.Instance);
                Project project = new Project();
                IList<Project> projects = uow.Projects.getAllOrderBy(project, "Date");

                foreach (var item in projects)
                {
                    TableRow tRow = new TableRow();
                    TableProjects.Rows.Add(tRow);

                    TableCell tCellId = new TableCell();
                    tCellId.Text = item.Id.ToString();
                    TableCell tCellNombre = new TableCell();
                    tCellNombre.Text = item.Name.ToString();
                    TableCell tCellCarrera = new TableCell();
                    tCellCarrera.Text = item.Carreer.Name;
                    TableCell tCellFecha = new TableCell();
                    tCellFecha.Text = item.Date.ToString();
                    TableCell tCellStatus = new TableCell();
                    if (item.Enable)
                    {
                        tCellStatus.Text = "Activo";
                    }
                    else
                    {
                        tCellStatus.Text = "Baja";
                    }
                    TableCell tCellUsuario = new TableCell();
                    tCellUsuario.Text = item.User.Name + " " + item.User.Lastname;

                    tRow.Cells.Add(tCellId);
                    tRow.Cells.Add(tCellNombre);
                    tRow.Cells.Add(tCellCarrera);
                    tRow.Cells.Add(tCellFecha);
                    tRow.Cells.Add(tCellStatus);
                    tRow.Cells.Add(tCellUsuario);
                }
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
    }
}