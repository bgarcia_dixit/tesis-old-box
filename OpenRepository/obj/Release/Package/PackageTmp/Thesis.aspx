﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Thesis.aspx.cs" Inherits="OpenRepository.Thesis" %>

<%@ Register Assembly="DevExpress.XtraReports.v14.1.Web, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header" style="text-align: center;">
        <h1>
            <asp:Label runat="server" ID="titulo"></asp:Label></h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">Resumen</div>
        <div class="panel-body">
            <asp:Label runat="server" ID="resumen"></asp:Label>
        </div>
    </div>
    <div style="text-align: center; position: relative;">
        <div class="sideTesis">
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic" style="margin-top: 5%;">
                <asp:Image ID="ImageProfile" runat="server" ImageUrl="~/Img/Resources/Perfil.jpg" CssClass="img-responsive" />
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
                <div class="profile-usertitle-name">
                    <asp:HyperLink ID="LabelProfileName" runat="server" Text="Perfil Incorrecto"></asp:HyperLink>
                </div>

            </div>
        </div>
        <iframe class="PDF" runat="server" id="PdfViewer" ></iframe>
    </div>
</asp:Content>
