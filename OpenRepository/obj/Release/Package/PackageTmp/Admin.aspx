﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="OpenRepository.Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h3>Panel de administración</h3>

    <div id="cfg-div" class="adminContainer">
        <div class="panel panel-default">
                    <div class="panel-heading">Carreras</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-lg-10">
                                <asp:TextBox ID="TextBoxNewCarreer" class="form-control" placeholder="Nueva Carrera" runat="server"></asp:TextBox>
                            </div>
                            <asp:Button ID="BtnAddCarreer" CssClass="btn btn-primary" runat="server" Text="Agregar" OnClick="BtnAddCarreer_Click" />
                        </div>
                        <div class="form-group">
                            <asp:ListBox ID="ListBoxCarreers" CssClass="form-control" runat="server" Rows="7"></asp:ListBox>
                        </div>
                        <div class="form-group">
                            <asp:Button ID="BtnDelete" runat="server" Text="Borrar Carrera" OnClick="BtnDelete_Click" />
                        </div>
                    </div>
                </div>
    </div>
    <div class="adminContainer">
        <asp:Table ID="TableUsers" runat="server" class="table table-striped table-hover ">
                    <asp:TableRow CssClass="info">
                        <asp:TableCell>#</asp:TableCell>
                        <asp:TableCell>Nombre</asp:TableCell>
                        <asp:TableCell>Status</asp:TableCell>
                        <asp:TableCell>Control</asp:TableCell>
                        <asp:TableCell>Administrador</asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
    </div>

    <div>
        <asp:Table ID="TableProject" runat="server" class="table table-striped table-hover ">
        <asp:TableRow CssClass="info">
            <asp:TableCell>Titulo</asp:TableCell>
            <asp:TableCell>Carrera</asp:TableCell>
            <asp:TableCell>Acciones</asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </div>
</asp:Content>
