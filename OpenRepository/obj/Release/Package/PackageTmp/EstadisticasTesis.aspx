﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="EstadisticasTesis.aspx.cs" Inherits="OpenRepository.EstadisticasTesis" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>Tabla de Tesis</h3>
    <div>
        <asp:HyperLink ID="BackLink" CssClass="btn btn-default" runat="server" NavigateUrl="~/Estadisticas.aspx">Atras</asp:HyperLink>
    </div>
    <asp:Table ID="TableProjects" runat="server" class="table table-striped table-hover ">
        <asp:TableRow CssClass="info">
            <asp:TableCell>ID</asp:TableCell>
            <asp:TableCell>Nombre</asp:TableCell>
            <asp:TableCell>Carrera</asp:TableCell>
            <asp:TableCell>Fecha Registro</asp:TableCell>
            <asp:TableCell>Estado</asp:TableCell>
            <asp:TableCell>Usuario</asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
