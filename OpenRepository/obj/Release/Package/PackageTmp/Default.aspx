﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="OpenRepository.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-xs-12">
            <asp:HyperLink ID="NewThesisLink" runat="server" NavigateUrl="~/NewUpload.aspx">
                <asp:Image ID="NewThesisBanner" runat="server" CssClass="img-thumbnail" ImageUrl="~/Img/Resources/PortadaOldBox.png" />
            </asp:HyperLink>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Filtrado de tesis</div>
        <div class="panel-body">
            <div class="form-group">
                <label for="inputCarrera" class="col-lg-2 control-label">Carrera</label>
                <div class="col-lg-7">
                    <asp:DropDownList ID="DropDownListCarreras" CssClass="form-control" runat="server"></asp:DropDownList>
                </div>
                <div class="col-lg-1">
                    <asp:Button ID="clearfilt" runat="server" class="btn btn-primary" type="button" Text="Borrar Filtro" OnClick="borrarFiltro"/>
                </div>
                <div class="col-lg-1 col-lg-offset-1">
                    <asp:Button ID="filtrarb" runat="server" class="btn btn-primary" type="button" Text="Filtrar" OnClick="filtrar"/>
                </div>
            </div>
        </div>
    </div>

    <asp:Table ID="TableProject" runat="server" class="table table-striped table-hover ">
        <asp:TableRow CssClass="info">
            <asp:TableCell>Titulo</asp:TableCell>
            <asp:TableCell>Carrera</asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Content>
