﻿using OpenRepository.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OpenRepository
{
    public partial class EstadisticasUsuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session.Count > 0 && int.Parse(Session["status"].ToString()) > 1)
            {
                UnitOfWork uow = new UnitOfWork(SessionManager.Instance);
                Usuario usuario = new Usuario();
                IList<Usuario> usuarios = uow.Users.getAllOrderBy(usuario, "Date");

                foreach (var item in usuarios)
                {
                    TableRow tRow = new TableRow();
                    TableUsers.Rows.Add(tRow);

                    TableCell tCellId = new TableCell();
                    tCellId.Text = item.Id.ToString();

                    TableCell tCellNombre = new TableCell();
                    tCellNombre.Text = item.Name.ToString();
                    TableCell tCellApellido = new TableCell();
                    tCellApellido.Text = item.Lastname.ToString();
                    TableCell tCellFecha = new TableCell();
                    tCellFecha.Text = item.Date.ToString();
                    TableCell tCellCantTesis = new TableCell();
                    long cantTesis = uow.Projects.getUser(item);
                    tCellCantTesis.Text = cantTesis.ToString();

                    TableCell tCellStatus = new TableCell();
                    switch (item.Status)
                    {
                        case 0:
                            {
                                tCellStatus.Text = "Bloqueado";
                                break;
                            }
                        case 1:
                            {
                                tCellStatus.Text = "Miembro";
                                break;
                            }
                        case 2:
                            {
                                tCellStatus.Text = "Administrador";
                                break;
                            }
                        case 3:
                            {
                                tCellStatus.Text = "Super Administrador";
                                break;
                            }
                        default:
                            {
                                tCellStatus.Text = "";
                                break;
                            }
                    }

                    tRow.Cells.Add(tCellId);
                    tRow.Cells.Add(tCellNombre);
                    tRow.Cells.Add(tCellApellido);
                    tRow.Cells.Add(tCellFecha);
                    tRow.Cells.Add(tCellStatus);
                    tRow.Cells.Add(tCellCantTesis);
                }
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
    }
}