﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="OpenRepository.Search" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div runat="server" id="userSearchDiv" visible="false" class="adminContainer">
        <div class="panel panel-default">
            <div class="panel-heading">Usuarios</div>
            <div class="panel-body">
                <asp:Table ID="TableUsers" runat="server" CssClass="table table-striped table-hover ">
                    <asp:TableRow CssClass="info">
                        <asp:TableCell>Nombre</asp:TableCell>
                        <asp:TableCell>Email</asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </div>
    </div>

    <div runat="server" id="projectSearchDiv" visible="false" class="adminContainer">
        <div class="panel panel-default">
            <div class="panel-heading">Tesis</div>
            <div class="panel-body">
                <asp:Table ID="TableProjects" runat="server" CssClass="table table-striped table-hover ">
                    <asp:TableRow CssClass="info">
                        <asp:TableCell>Nombre</asp:TableCell>
                        <asp:TableCell>Carrera</asp:TableCell>
                        <asp:TableCell>Autor</asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </div>
    </div>

</asp:Content>
