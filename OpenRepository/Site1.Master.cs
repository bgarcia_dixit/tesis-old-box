﻿using OpenRepository.Clases;
using OpenRepository.DAO;
using OpenRepository.DAO.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OpenRepository
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SesionDiv.Visible = false;
            HyperLinkLogin.Visible = true;
            HyperLinkRegistro.Visible = true;
            AlertErrorLoginDiv.Visible = false;

            //verifico sesion
            if (Session["status"] != null)
            {
                if (Convert.ToInt32(Session["status"].ToString()) != 0)
                {
                    SesionDiv.Visible = true;
                    HyperLinkLogin.Visible = false;
                    HyperLinkRegistro.Visible = false;
                    BtnPerfil.Text = Session["name"].ToString() + " " + Session["lastname"].ToString();

                    //link admin
                    if (Int16.Parse(Session["status"].ToString()) >= 2)
                    {
                        adminPageLink.Visible = true;
                        estadisticasPageLink.Visible = true;
                    }
                }
            }
        }

        protected void BtnLogin_Click(object sender, EventArgs e)
        {
            UnitOfWork uow = new UnitOfWork(SessionManager.Instance);
            Usuario sesion = uow.Users.readAll(username.Text, password.Text);

            if (sesion != null)
            {
                if (sesion.Status != 0)
                {
                    Session["username"] = sesion.Email;
                    Session["status"] = sesion.Status;
                    Session["name"] = sesion.Name;
                    Session["lastname"] = sesion.Lastname;

                    BtnPerfil.Text = Session["name"].ToString() + " " + Session["lastname"].ToString();
                    SesionDiv.Visible = true;
                    HyperLinkLogin.Visible = false;
                    HyperLinkRegistro.Visible = false;

                    //link admin
                    if (Int16.Parse(Session["status"].ToString()) >= 2)
                    {
                        adminPageLink.Visible = true;
                        estadisticasPageLink.Visible = true;
                    }

                    Response.Redirect("Default.aspx");
                }
                else
                {
                    Response.Redirect("Profile.aspx?userProfile=" + sesion.Email);
                    Session.Abandon();
                }
            }
            else
            {
                AlertErrorLoginDiv.Visible = true;
            }
        }

        protected void lnkLogoutUser_Click(object sender, EventArgs e)
        {
            SesionDiv.Visible = false;
            HyperLinkLogin.Visible = true;
            HyperLinkRegistro.Visible = true;
            Session.Abandon();
            Response.Redirect("Default.aspx");
        }

        protected void BtnPerfil_Click(object sender, EventArgs e)
        {
            String userProfile = Session["username"].ToString();
            Response.Redirect("Profile.aspx?userProfile=" + userProfile);
        }

        protected void search()
        {
            Response.Redirect("Search.aspx?word=" + SearchTextBox.Text);
        }

        protected void SearchBtn_Click(object sender, EventArgs e)
        {
            search();
        }

        protected void SearchTextBox_Enter(object sender, EventArgs e)
        {
            search();
        }
    }
}