﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenRepository.Clases
{
    public class Ban
    {
        private long id;
        private Usuario user;
        private Usuario admin;
        private DateTime date;

        public virtual long Id
        {
            get { return id; }
            set { id = value; }
        }

        public virtual Usuario User
        {
            get { return user; }
            set { user = value; }
        }

        public virtual Usuario Admin
        {
            get { return admin; }
            set { admin = value; }
        }

        public DateTime Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }
    }
}