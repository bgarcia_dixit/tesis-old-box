﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Context;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;

namespace OpenRepository.Clases
{
    public sealed class SessionManager
    {
        private static ISessionFactory instance = null;
        private static readonly object padlock = new object();

        private SessionManager() { }

        public static ISessionFactory Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        Configuration configuration = new Configuration(); // Namespace = NHibernate.Cfg
                        configuration.Configure(System.Web.HttpContext.Current.Server.MapPath("~/hibernate.cfg.xml"));
                        instance = configuration.BuildSessionFactory();
                    }

                    return instance;
                }
            }
        }
    }
}