﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenRepository.Clases
{
    public class Project
    {
        private long id;
        private string name;
        private DateTime date;
        private Usuario user;
        // CAMBIAR A BOOLEANO = HABILITADO O BASURA
        //private int type; //0 = trash / 1 = thesis / 2 = project
        private bool enable;
        private string summary;
        private Career carreer;
        IList<Archive> archivesList;

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual IList<Archive> ArchivesList
        {
            get { return archivesList; }
            set { archivesList = value; }
        }

        public virtual string Summary
        {
            get { return summary; }
            set { summary = value; }
        }

        public virtual Usuario User
        {
            get { return user; }
            set { user = value; }
        }

        public virtual long Id
        {
            get { return id; }
            set { id = value; }
        }

        public Career Carreer
        {
            get
            {
                return carreer;
            }

            set
            {
                carreer = value;
            }
        }

        public bool Enable
        {
            get
            {
                return enable;
            }

            set
            {
                enable = value;
            }
        }

        public DateTime Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }
    }
}