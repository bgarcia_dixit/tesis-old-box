﻿using NHibernate;
using OpenRepository.DAO.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenRepository.Clases
{
    public class UnitOfWork : IDisposable
    {
        private ITransaction transaction;
        private ISession session;
        private bool disposed = false;
        private readonly ISessionFactory sessionFactory;

        public UnitOfWork(ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;
        }

        public void BeginTransaction()
        {
            if (this.transaction == null)
                this.transaction = this.session.BeginTransaction();
        }

        public void Commit()
        {
            if (this.transaction != null)
                this.transaction.Commit();
        }

        public void Rollback()
        {
            if (this.transaction != null)
                this.transaction.Rollback();
        }

        private ISession GetSession()
        {
            if (this.session == null)
                this.session = this.sessionFactory.OpenSession();

            return this.session;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (this.transaction != null)
                        this.transaction.Dispose();

                    if (this.session != null)
                        this.session.Close();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        //clases
        private ProjectDaoImpl project;
        public ProjectDaoImpl Projects
        {
            get
            {
                if (this.project == null)
                    this.project = new ProjectDaoImpl(this.GetSession());

                return this.project;
            }
        }

        private UsuarioDaoImpl user;
        public UsuarioDaoImpl Users
        {
            get
            {
                if (this.user == null)
                    this.user = new UsuarioDaoImpl(this.GetSession());

                return this.user;
            }
        }

        private BanDaoImpl ban;
        public BanDaoImpl Ban
        {
            get
            {
                if (this.ban == null)
                    this.ban = new BanDaoImpl(this.GetSession());

                return this.ban;
            }
        }

        private CareerDaoImpl carreer;
        public CareerDaoImpl Carreer
        {
            get
            {
                if (this.carreer == null)
                    this.carreer = new CareerDaoImpl(this.GetSession());

                return this.carreer;
            }
        }
    }
}