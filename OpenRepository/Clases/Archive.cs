﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenRepository.Clases
{
    public class Archive
    {
        private long id;
        private string name;
        private string description;
        private Project project;

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual Project Project
        {
            get { return project; }
            set { project = value; }
        }

        public virtual long Id
        {
            get { return id; }
            set { id = value; }
        }
    }
}