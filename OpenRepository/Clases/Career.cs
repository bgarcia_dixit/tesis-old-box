﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenRepository.Clases
{
    public class Career
    {
        private long id;
        private string name;

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual long Id
        {
            get { return id; }
            set { id = value; }
        }

    }
}