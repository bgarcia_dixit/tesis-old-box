﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenRepository.Clases
{
    public class Usuario
    {
        private long id;
        private string email;
        private string password;
        private int status;
        private string name;
        private string lastname;
        private string imgProfile;
        private IList<Project> projects;
        private DateTime date;

        public virtual IList<Project> Projects
        {
            get { return projects; }
            set { projects = value; }
        }

        public virtual string ImgProfile
        {
            get { return imgProfile; }
            set { imgProfile = value; }
        }

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual string Lastname
        {
            get { return lastname; }
            set { lastname = value; }
        }

        public virtual int Status
        {
            get { return status; }
            set { status = value; }
        }

        public virtual long Id
        {
            get { return id; }
            set { id = value; }
        }

        public virtual string Email
        {
            get { return email; }
            set { email = value; }
        }

        public virtual string Password
        {
            get { return password; }
            set { password = value; }
        }

        public DateTime Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }
    }
}