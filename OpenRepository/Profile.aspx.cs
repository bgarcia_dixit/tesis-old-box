﻿using OpenRepository.Clases;
using OpenRepository.DAO;
using OpenRepository.DAO.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace OpenRepository
{
    public partial class Profile : System.Web.UI.Page
    {
        private String userProfile = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            userProfile = Request.QueryString["userProfile"];
            UnitOfWork uow = new UnitOfWork(SessionManager.Instance);

            //BUSCO USUARIO PARA MOSTRAR PERFIL
            Usuario profile = uow.Users.buscarPerfil(userProfile);

            //DIVS PERFIL
            ProfileCfgDiv.Visible = false;
            SidePrincipal.Attributes.Add("class", "active");
            SideCfg.Attributes.Remove("class");

            //ordeno la pagina
            ProfileCfgDiv.Visible = false;
            SideCfg.Visible = false;

            //SI EL PERFIL ES VALIDO APARECEN LAS COSITAS
            if (profile != null)
            {
                Page.Title += "OldBox: " + profile.Name + " " + profile.Lastname;
                LabelProfileName.Text = profile.Name + " " + profile.Lastname;

                LabelEmail.Text = profile.Email;

                //si el perfil es del usuario de la sesion puede ver la configuracion
                if (Session.Count > 0)
                {
                    if (Session["username"].ToString() == profile.Email)
                    {
                        SideCfg.Visible = true;
                    }
                }
                else
                {
                    SideCfg.Visible = false;
                }

                //busco foto de perfil
                if (profile.ImgProfile != null)
                {
                    ImageProfile.ImageUrl = profile.ImgProfile;
                }
                else
                {
                    ImageProfile.ImageUrl = "~\\Img\\Resources\\Perfil.jpg";
                }

                switch (profile.Status)
                {
                    case 0:
                        {
                            LabelProfileStatus.Text = "BLOQUEADO";
                            break;
                        }
                    case 1:
                        {
                            LabelProfileStatus.Text = "Miembro";
                            break;
                        }
                    case 2:
                        {
                            LabelProfileStatus.Text = "Administrador";
                            break;
                        }
                    case 3:
                        {
                            LabelProfileStatus.Text = "Super Administrador";
                            break;
                        }
                    default:
                        {
                            LabelProfileStatus.Text = "";
                            break;
                        }
                }
                //si el perfil tiene bloqueo
                if (profile.Status == 0)
                {
                    Ban bloqueo = uow.Ban.searchBan(profile);

                    LabelAdmin.Text = bloqueo.Admin.Name + " " + bloqueo.Admin.Lastname;
                    LabelEmailAdmin.Text = bloqueo.Admin.Email;

                    if (LabelEmailAdmin.Text != "alexis_bianchi@hotmail.com")
                    {
                        divSAdmin.Visible = true;
                    }
                    //muestro div
                    divBan.Visible = true;
                }

                //acomodo pantalla de configuraciones
                if (!Page.IsPostBack)
                {
                    TextBoxName.Text = profile.Name;
                    TextBoxLastName.Text = profile.Lastname;
                    TextBoxEmail.Text = profile.Email;
                }
            }
            else
            {
                LabelProfileName.Text = "Perfil no registrado";
                LabelProfileStatus.Text = "";
            }


        }

        protected void LinkButtonMain_Click(object sender, EventArgs e)
        {
            //DIVS PERFIL
            ProfileMain.Visible = true;
            ProfileCfgDiv.Visible = false;
            SidePrincipal.Attributes.Add("class", "active");
            SideCfg.Attributes.Remove("class");
        }

        protected void LinkButtonCfg_Click(object sender, EventArgs e)
        {
            //DIVS PERFIL
            ProfileMain.Visible = false;
            ProfileCfgDiv.Visible = true;
            SidePrincipal.Attributes.Remove("class");
            SideCfg.Attributes.Add("class", "active");
        }

        protected void LinkButtonRep_Click(object sender, EventArgs e)
        {
            //DIVS PERFIL
            ProfileMain.Visible = false;
            ProfileCfgDiv.Visible = false;
            SidePrincipal.Attributes.Remove("class");
            SideCfg.Attributes.Remove("class");
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                UnitOfWork uow = new UnitOfWork(SessionManager.Instance);
                Usuario userModificar = uow.Users.buscarPerfil(Session["username"].ToString());

                userModificar.Name = TextBoxName.Text;
                userModificar.Lastname = TextBoxLastName.Text;

                LabelProfileName.Text = TextBoxName.Text + " " + TextBoxLastName.Text;

                if (FileUploadImgProfile.HasFile == true)
                {
                    String ImgProfileDir = "\\Img\\Profile\\";

                    DirectoryInfo dir = new DirectoryInfo(Server.MapPath(ImgProfileDir));

                    //borrar si ya existe una imagen
                    FileInfo[] files = dir.GetFiles(Session["username"].ToString() + ".*");
                    if (files.Length > 0)
                    {
                        //File exists
                        foreach (FileInfo file in files)
                        {
                            File.Delete(file.FullName.ToString());
                        }
                    }


                    try
                    {
                        FileUploadImgProfile.PostedFile.SaveAs(Server.MapPath(ImgProfileDir) + Session["username"].ToString() + Path.GetExtension(FileUploadImgProfile.FileName));

                        userModificar.ImgProfile = ImgProfileDir + Session["username"].ToString() + Path.GetExtension(FileUploadImgProfile.FileName);
                        ImageProfile.ImageUrl = userModificar.ImgProfile;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                uow.Users.update(userModificar);
            }

            //Recargo en la pantalla de configuraciones
            ProfileCfgDiv.Visible = true;
            SidePrincipal.Attributes.Remove("class");
            SideCfg.Attributes.Add("class", "active");
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            TextBoxName.Text = Session["name"].ToString();
            TextBoxLastName.Text = Session["lastname"].ToString();

            ProfileCfgDiv.Visible = false;
            SidePrincipal.Attributes.Add("class", "active");
            SideCfg.Attributes.Remove("class");
        }
    }
}