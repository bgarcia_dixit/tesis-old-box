﻿using OpenRepository.Clases;
using OpenRepository.DAO;
using OpenRepository.DAO.Impl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OpenRepository
{
    public partial class NewUpload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session.Count == 0)
            {
                ClientScriptManager CSM = Page.ClientScript;
                string strconfirm = "<script>if(!window.alert('Debe iniciar seción para subir una tesis')){ window.location.href='Default.aspx' }</script>";
                CSM.RegisterClientScriptBlock(this.GetType(), "alert", strconfirm, false);
                
            }
            else
            {
                if (!Page.IsPostBack)
                {
                    UnitOfWork uow = new UnitOfWork(SessionManager.Instance);
                    //cargo la pagina
                    IList<Career> carreerList = uow.Carreer.getAllOrderBy(new Career(), "Name");

                    DropDownListCarreras.DataSource = carreerList;
                    DropDownListCarreras.DataTextField = "Name";
                    DropDownListCarreras.DataValueField = "Id";
                    DropDownListCarreras.DataBind();
                }
            }
        }

        protected void BtnUploadProject_Click(object sender, EventArgs e)
        {
            UnitOfWork uow = new UnitOfWork(SessionManager.Instance);
            Project newProject = new Project();
            

            newProject.Name = InputTitle.Text;

            newProject.User = uow.Users.buscarPerfil(Session["username"].ToString());
            newProject.Summary = textAreaSummary.Value;
            newProject.Carreer = uow.Carreer.get(long.Parse(DropDownListCarreras.SelectedValue));
            newProject.Date = DateTime.Now;

            //Sacar para la habilitar la revisión
            newProject.Enable = true;

            if (FileUploadMainPDF.HasFile == true)
            {
                String PdfDir = "\\Archives\\";

                DirectoryInfo dir = new DirectoryInfo(Server.MapPath(PdfDir));
                
                try
                {
                    Archive PDF = new Archive();
                    PDF.Name = newProject.Name + Path.GetExtension(FileUploadMainPDF.FileName);
                    FileUploadMainPDF.PostedFile.SaveAs(Server.MapPath(PdfDir) + newProject.Name + Path.GetExtension(FileUploadMainPDF.FileName));
                    
                    newProject.ArchivesList = new List<Archive>();
                    newProject.ArchivesList.Add(PDF);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            uow.Projects.save(newProject);
            Response.Redirect("~/Default.aspx");
        }
    }
}