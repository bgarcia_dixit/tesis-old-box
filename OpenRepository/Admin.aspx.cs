﻿using OpenRepository.Clases;
using OpenRepository.DAO;
using OpenRepository.DAO.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace OpenRepository
{
    public partial class Admin : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session.Count > 0 && int.Parse(Session["status"].ToString()) > 1)
            {
                UnitOfWork uow = new UnitOfWork(SessionManager.Instance);
                IList<Usuario> usuarios = uow.Users.readAll(Int16.Parse(Session["status"].ToString()));

                long i = 1;

                foreach (Usuario item in usuarios)
                {
                    Button btn = new Button();
                    Button btnAdmin = new Button();

                    TableRow tRow = new TableRow();
                    TableUsers.Rows.Add(tRow);

                    TableCell tCellId = new TableCell();
                    tCellId.Text = item.Id.ToString();


                    TableCell tCellNombre = new TableCell();
                    HyperLink link = new HyperLink();
                    link.NavigateUrl = "~/Profile.aspx?userProfile=" + item.Email;
                    link.Text = item.Name + " " + item.Lastname;
                    tCellNombre.Controls.Add(link);

                    TableCell tCellStatus = new TableCell();
                    switch (item.Status)
                    {
                        case 0:
                            {
                                tCellStatus.Text = "BLOQUEADO";
                                btn.Text = "Desbloquear";
                                btn.CssClass = "btn btn-success btn-sm";

                                btnAdmin.CssClass = "btn btn-danger disabled btn-sm";
                                btnAdmin.Text = "NO";
                                btnAdmin.Enabled = false;
                                break;
                            }
                        case 1:
                            {
                                tCellStatus.Text = "Miembro";
                                btn.Text = "Bloquear";
                                btn.CssClass = "btn btn-danger btn-sm";
                                if (int.Parse(Session["status"].ToString()) == 3)
                                {
                                    btnAdmin.CssClass = "btn btn-danger btn-sm";
                                }
                                else
                                {
                                    btnAdmin.CssClass = "btn btn-danger btn-sm disabled";
                                    btnAdmin.Enabled = false;
                                }
                                btnAdmin.Text = "NO";
                                break;
                            }
                        case 2:
                            {
                                tCellStatus.Text = "Administrador";
                                btn.Text = "Bloquear";
                                btn.CssClass = "btn btn-danger btn-sm";

                                btnAdmin.CssClass = "btn btn-success btn-sm";
                                btnAdmin.Text = "SI";
                                break;
                            }
                        case 3:
                            {
                                tCellStatus.Text = "Super Administrador";
                                break;
                            }
                        default:
                            {
                                tCellStatus.Text = "";
                                break;
                            }
                    }

                    TableCell tCellControl = new TableCell();
                    btn.ID = item.Email.ToString();
                    btn.Attributes.Add("row", i.ToString());
                    btn.Click += new EventHandler(btn_ClickBan);
                    tCellControl.Controls.Add(btn);

                    TableCell tCellAdmin = new TableCell();
                    btnAdmin.Attributes.Add("idUsuario", item.Email.ToString());
                    btnAdmin.Attributes.Add("row", i.ToString());
                    btnAdmin.Click += new EventHandler(btnAdmin_Clic);
                    tCellAdmin.Controls.Add(btnAdmin);

                    tRow.Cells.Add(tCellId);
                    tRow.Cells.Add(tCellNombre);
                    tRow.Cells.Add(tCellStatus);
                    tRow.Controls.Add(tCellControl);
                    tRow.Cells.Add(tCellAdmin);

                    i++;
                }
                
                //lista de carreras
                if (!Page.IsPostBack)
                {
                    IList<Career> carreras = uow.Carreer.getAll(new Career());
                    ListBoxCarreers.DataSource = carreras;
                    ListBoxCarreers.DataTextField = "Name";
                    ListBoxCarreers.DataValueField = "Id";
                    ListBoxCarreers.DataBind();
                }

                IList<Project> projectsList = uow.Projects.getAll(new Project(), " c.Enable = TRUE ");
                foreach (Project item in projectsList)
                {
                    long rowIndex = 1;
                    Button btnDelete = new Button();

                    TableRow tRow = new TableRow();
                    TableProject.Rows.Add(tRow);

                    TableCell tCellProject = new TableCell();
                    HyperLink link = new HyperLink();
                    link.NavigateUrl = "~/Thesis.aspx?thesisID=" + item.Id;

                    link.Text = item.Name;
                    tCellProject.Controls.Add(link);

                    TableCell tCellCarreer = new TableCell();
                    tCellCarreer.Text = item.Carreer.Name;

                    TableCell tCellAcciones = new TableCell();
                    btnDelete.ID = item.Id.ToString();
                    btnDelete.Attributes.Add("idRow", rowIndex.ToString());
                    btnDelete.CssClass = "btn btn-danger btn-sm";
                    btnDelete.Text = "Dar de baja";
                    btnDelete.Click += new EventHandler(btnDeleteThesis_Click);
                    tCellAcciones.Controls.Add(btnDelete);


                    tRow.Cells.Add(tCellProject);
                    tRow.Cells.Add(tCellCarreer);
                    tRow.Cells.Add(tCellAcciones);

                    rowIndex++;
                }
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        void btnDeleteThesis_Click(object sender, EventArgs e)
        {
            UnitOfWork uow = new UnitOfWork(SessionManager.Instance);

            Project delteProject = uow.Projects.get(long.Parse(((Button)sender).ID));
            delteProject.Enable = false;
            uow.Projects.update(delteProject);

            TableProject.Rows.RemoveAt(int.Parse(((Button)sender).Attributes["idRow"]));
        }

        void btn_ClickBan(object sender, EventArgs e)
        {
            UnitOfWork uow = new UnitOfWork(SessionManager.Instance);
            Usuario usuario = uow.Users.buscarPerfil(((Button)sender).ID);

            if (((Button)sender).Text == "Desbloquear")
            {
                //unban user
                usuario.Status = 1;
                uow.Users.update(usuario);
                uow.Ban.delete(uow.Ban.searchBan(usuario));
                ((Button)sender).Text = "Bloquear";
                ((Button)sender).CssClass = "btn btn-danger btn-sm";
                TableUsers.Rows[Int32.Parse(((Button)sender).Attributes["row"].ToString())].Cells[2].Text = "Miembro";
            }
            else
            {
                //ban user
                usuario.Status = 0;
                uow.Users.update(usuario);

                Ban constancia = new Ban();
                constancia.Admin = uow.Users.buscarPerfil(Session["username"].ToString());
                constancia.User = usuario;
                constancia.Date = DateTime.Now;
                uow.Ban.save(constancia);

                ((Button)sender).Text = "Desbloquear";
                ((Button)sender).CssClass = "btn btn-success btn-sm";
                TableUsers.Rows[Int32.Parse(((Button)sender).Attributes["row"].ToString())].Cells[2].Text = "BLOQUEADO";
            }
        }

        void btnAdmin_Clic(object sender, EventArgs e)
        {
            UnitOfWork uow = new UnitOfWork(SessionManager.Instance);
            Usuario usuario = uow.Users.buscarPerfil(((Button)sender).Attributes["idUsuario"].ToString());

            if (usuario.Status == 2)
            {
                usuario.Status = 1;
                uow.Users.update(usuario);
                ((Button)sender).CssClass = "btn btn-danger btn-sm";
                ((Button)sender).Text = "NO";
                TableUsers.Rows[Int32.Parse(((Button)sender).Attributes["row"].ToString())].Cells[2].Text = "Miembro";
            }
            else
            {
                usuario.Status = 2;
                uow.Users.update(usuario);
                ((Button)sender).CssClass = "btn btn-success btn-sm";
                ((Button)sender).Text = "SI";
                TableUsers.Rows[Int32.Parse(((Button)sender).Attributes["row"].ToString())].Cells[2].Text = "Administrador";
            }
        }

        protected void BtnAddCarreer_Click(object sender, EventArgs e)
        {
            UnitOfWork uow = new UnitOfWork(SessionManager.Instance);

            Career newCarreer = new Career();

            newCarreer.Name = TextBoxNewCarreer.Text;
            uow.Carreer.save(newCarreer);

            TextBoxNewCarreer.Text = "";

            IList<Career> carreras = uow.Carreer.getAll(new Career());

            ListBoxCarreers.DataSource = carreras;
            ListBoxCarreers.DataTextField = "Name";
            ListBoxCarreers.DataValueField = "Id";
            ListBoxCarreers.DataBind();
        }

        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            UnitOfWork uow = new UnitOfWork(SessionManager.Instance);
            Career carreraBorrar = uow.Carreer.search(long.Parse(ListBoxCarreers.SelectedValue));
            uow.Carreer.delete(carreraBorrar);
            
            IList<Career> carreras = uow.Carreer.getAll(new Career());
            ListBoxCarreers.DataSource = carreras;
            ListBoxCarreers.DataTextField = "Name";
            ListBoxCarreers.DataValueField = "Id";
            ListBoxCarreers.DataBind();
        }
    }
}